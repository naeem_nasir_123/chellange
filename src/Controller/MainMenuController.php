<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainMenuController extends AbstractController
{
    /**
     * @Route("/", name="main_menu")
     */
    public function index()
    {
        return $this->render('main_menu/mainMenu.html.twig');
    }
}
