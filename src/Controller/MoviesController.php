<?php

namespace App\Controller;

use App\BusinessLogic\Movies\MoviesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MoviesController extends AbstractController
{
    /**
     * @Route("/api/v1/import", name="movies_imports")
     */
    public function import(MoviesService $moviesService)
    {
        $result = $moviesService->ImportMovies();
        $response = new JsonResponse();
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;
    }

    /**
     * @Route("/list-movies", name="movies_list")
     */
    public function listMovies(Request $request, MoviesService $moviesService)
    {
        $movies = $moviesService->getPaginatedMovies($request->get('page', 1), 5);

        return $this->render('movies/listMovies.html.twig', ['movies' => $movies]);
    }

    /**
     * @Route("/movie/details", name="movie_details")
     */
    public function movieDetails(Request $request, MoviesService $moviesService)
    {
        $movie = $moviesService->getMovieById($request->get('movie_id'));

        return $this->render('movies/movieDetails.html.twig', ['movie' => $movie]);
    }

    /**
     * @Route("/movie/add-page", name="movie_add-page")
     */
    public function movieAddPage(Request $request, MoviesService $moviesService)
    {
        return $this->render('movies/addMovies.html.twig');
    }

    /**
     * @Route("/movie/add", name="movie_add")
     * @Method({"POST"})
     */
    public function addMovie(Request $request, MoviesService $moviesService)
    {
        $result = $moviesService->addMovie($request);
        $response = new JsonResponse();
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;
    }

    /**
     * @Route("/movie/remove", name="movie_remove")
     * @Method({"DELETE"})
     */
    public function removeMovie(Request $request, MoviesService $moviesService)
    {
        $result = $moviesService->removeMovie($request);
        $response = new JsonResponse();
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;
    }

    /**
     * @Route("/movie/edit", name="movie_edit")
     * @Method({"POST"})
     */
    public function editMovie(Request $request, MoviesService $moviesService)
    {
        $result = $moviesService->editMovie($request);
        $response = new JsonResponse();
        $response->setData($result['data']);
        $response->setStatusCode($result['status']);

        return $response;
    }


    /**
     * @Route("/movies/manage", name="movie_manage")
     */
    public function manageMovies(Request $request, MoviesService $moviesService)
    {
        $movies = $moviesService->getPaginatedMovies($request->get('page', 1), 5);

        return $this->render('movies/manageMovies.html.twig', ['movies' => $movies]);
    }
}
